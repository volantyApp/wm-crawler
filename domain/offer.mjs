import { valueOf } from '../domain/advertiserTypeEnum.mjs';

class Offer {
    constructor(result) {
        this.offer = {
            id: result.id,
            name: result.name,
            externalId: result.externalId,
            basePrice: parseInt(result.basePrice),
            brand: result.brand,
            model: result.model,
            version: result.version,
            modelYear: result.modelYear,
            manufactoryYear: result.manufactoryYear,
            km: result.km,
            city: result.city,
            state: result.state,
            advertiser: valueOf(result.adType),
            offerDate: result.offerDate,
            createdAt: result.createdAt,
            updatedAt: result.updatedAt,
            contacts: result.contacts,
            color: result.color,
            doors: result.doors,
            gear: result.gear,
            fuel: result.fuel,
            bodyStyle: result.bodyStyle,
            extras: Array,
        }
    }

    get() {
        return this.offer
    }
}

export {
    Offer
}
