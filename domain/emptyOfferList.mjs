class EmptyOfferList {

    constructor(limit, offset) {
        this.emptyOfferList = {
            summary: {
                total: 0,
                limit: parseInt(limit),
                offset: parseInt(offset),
                minUpdatedDate: null,
                maxUpdatedDate: null
            },
            offers: {}
        }
    }

    get() {
        return this.emptyOfferList
    }
    
}

export {
    EmptyOfferList
}