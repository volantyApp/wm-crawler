const names = Object.freeze({
    AC: {value: "Acre (AC)", uf: "AC"},
    AL: {value: "Alagoas (AL)", uf: "AL"},
    AP: {value: "Amapá (AP)", uf: "AP"},
    AM: {value: "Amazonas (AM)", uf: "AM" },
    BA: {value: "Bahia (BA)", uf: "BA"},
    CE: {value: "Ceará (CE)", uf: "CE"},
    DF: {value: "Distrito Federal (DF)", uf: "DF"},
    ES: {value: "Espírito Santo (ES)", uf: "ES"},
    GO: {value: "Goiás (GO)", uf: "GO"},
    MA: {value: "Maranhão (MA)", uf: "MA"},
    MT: {value: "Mato Grosso (MT)", uf: "MT"},
    MS: {value: "Mato Grosso do Sul (MS)", uf: "MS"},
    MG: {value: "Minas Gerais (MG)", uf: "MG"},
    PA: {value: "Pará (PA)", uf: "PA"},
    PB: {value: "Paraíba (PB)", uf: "PB"},
    PR: {value: "Paraná (PR)", uf: "PR"},
    PE: {value: "Pernambuco (PE)", uf: "PE"},
    PI: {value: "Piauí (PI)", uf: "PI"},
    RJ: {value: "Rio de Janeiro (RJ)", uf: "RJ"},
    RN: {value: "Rio Grande do Norte (RN)", uf: "RN"},
    RS: {value: "Rio Grande do Sul (RS)", uf: "RS"},
    RO: {value: "Rondônia (RO)", uf: "RO"},
    RR: {value: "Roraima (RR)", uf: "RR"},
    SC: {value: "Santa Catarina (SC)", uf: "SC"},
    SP: {value: "São Paulo (SP)", uf: "SP"},
    SE: {value: "Sergipe (SE)", uf: "SE"},
    TO: {value: "Tocantins (TO)", uf: "TO"}
})

const from = (state) => {
    if (names[state]) {
        return names[state]
    }

    console.log(`UF não definido para o state: [${state}]`)
    throw new Error('UF não definido')
}

const valueOf = (value) => {
    const filtered = Object.keys(names).filter(key => value === names[key].value)

    if (filtered && filtered.length == 1) {
        return names[filtered[0]]
    }

    console.log(`UF não definido para o valor: [${value}]`)
    throw new Error('UF não definido')
}

export {
    names,
    from,
    valueOf
}