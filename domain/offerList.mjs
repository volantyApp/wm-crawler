import {Offer} from './offer.mjs'

class OfferList {

    constructor(result, total, limit, offset) {
        this.offerList = {
            summary: {
                total: total,
                limit: parseInt(limit),
                offset: parseInt(offset),
                minUpdatedDate: result.reduce(function (min, p) {
                    return p.updatedAt < min.updatedAt ? p : min
                }).updatedAt,
                maxUpdatedDate: result.reduce(function (max, p) {
                    return p.updatedAt > max.updatedAt ? p : max
                }).updatedAt
            },
            offers: result.map(o => new Offer(o).get())
        }
    }

    get() {
        return this.offerList
    }
    
}

export {
    OfferList
}