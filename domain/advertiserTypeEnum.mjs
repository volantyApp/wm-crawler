const names = Object.freeze({
    LOJA: {value: "Loja", id: 1},
    PF: {value: "Pessoa Física", id: 3},
    CONCESSIONARIA: {value: "Concessionária", id: 2}
})

const from = (advertiserType) => {
    if (names[advertiserType]) {
        return names[advertiserType] 
    }

    console.log(`AdvertiserType não definido para o type: [${advertiserType}]`)
    throw new Error('AdvertiserType não definido')
}

const valueOf = (value) => {
    const filtered = Object.keys(names).filter(key => value === names[key].value)

    if (filtered && filtered.length == 1) {
        return names[filtered[0]]
    }

    console.log(`AdvertiserType não definido para o valor: [${value}]`)
    throw new Error('AdvertiserType não definido')
}

export {
    names,
    from,
    valueOf
}