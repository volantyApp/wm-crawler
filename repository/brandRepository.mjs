import brandModel from "../model/brandModel.mjs"

const saveBrands = (brands) => {
    return brandModel.insertMany(brands)
            .then(r => {
                console.log('Brands inseridas com sucesso: ')
                return r
            })
            .catch(e => {
                console.log('Erro ao inserir Brands: ', e)
                Promise.reject(e)
            })
}

const getBrands = () => {
    return brandModel.find({})
            .then(r => {
                return r.map(b => b.brand)
            })
            .catch(e => {
                console.log('Erro ao recuperar as Brands: ', e)
                Promise.reject(e)
            })
}

export {
    saveBrands,
    getBrands
}