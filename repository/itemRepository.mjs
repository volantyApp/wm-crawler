import itemModel from "../model/itemModel.mjs"
import {OfferList} from '../domain/offerList.mjs'
import {EmptyOfferList} from '../domain/emptyOfferList.mjs'

const parseSortQuery = (sortQuery, prefix) => {
    if (sortQuery === undefined) {
        return {}
    }

    const fields = sortQuery.split(',')
    let sort = {}

    fields.forEach(field => {
        let splitResult = field.split('.')
        let key = prefix ? prefix + '.' + splitResult[0] : splitResult[0]
        let order = splitResult[1]

        if ('asc' === order) {
            sort[key] = 1
        } else {
            sort[key] = -1
        }
    })

    return sort
}

const GET_ITEMS_QUERY = (manufacturer, brand, city, state, kmFrom = 0, kmTo = 100000, priceFrom = 0, priceTo = 400000, yearFrom = 2010, yearTo = 2021, advertiserType) => {
    let query = {
        $and: [
            {brand: manufacturer}, 
            {state: state}, 
            {km: {$gte: parseInt(kmFrom), $lte: parseInt(kmTo)}},
            {modelYear: {$gte: parseInt(yearFrom), $lte: parseInt(yearTo)}},
            {basePrice: {$gte: parseInt(priceFrom), $lte: parseInt(priceTo)}}
        ]
    }

    if (brand) {
        query.$and.push({model: brand})
    }

    if (advertiserType) {
        query.$and.push({adType: advertiserType.value})
    }

    if (city) {
        query.$and.push({city: city})
    }

    return query
}

const COUNT_ITEMS_QUERY = (manufacturer, brand, city, state, kmFrom = 0, kmTo = 100000, priceFrom = 0, priceTo = 400000, yearFrom = 2010, yearTo = 2021, advertiserType) => {
    let query = []

    let match = {
        $match: {
            brand: manufacturer,
            state: state,
            km: {
                $gte: parseInt(kmFrom),
                $lte: parseInt(kmTo)
            },
            modelYear: {
                $gte: parseInt(yearFrom),
                $lte: parseInt(yearTo)
            },
            basePrice: {
                $gte: parseInt(priceFrom),
                $lte: parseInt(priceTo)
            }
        }
    }

    if (brand) {
        match.$match['model'] = brand
    }

    if (advertiserType) {
        match.$match['adType'] = advertiserType.value
    }

    if (city) {
        match.$match['city'] = city
    }

    let group = {
        $group: {
            _id: "$brand", 
            minPrice: {$min: "$basePrice"},
            maxPrice: {$max: "$basePrice"},
            avgPrice: {$avg: "$basePrice"},
            minUpdatedDate: {$min: "$updatedAt"},
            maxUpdatedDate: {$max: "$updatedAt"},
            minKm: {$min: "$km"},
            maxKm: {$max: "$km"},
            avgKm: {$avg: "$km"},
            total: {$sum: 1}
        }
    }

    query.push(match)
    query.push(group)

    return query
}

const parseResult = (result, total, limit, offset) => {
    if (result.length > 0) {
        return new OfferList(result, total, limit, offset).get()
    }
    return new EmptyOfferList(limit, offset).get()
}

const countDocuments = (query) => {
    return itemModel.countDocuments(query)
}

const calculateOffset = (offset, limit) => {
    return offset == 0 ? 0 : limit * (offset - 1)
}

const callGetItemsRepository = (query, limit, offset, sortBy) => {
    return itemModel.find(query).skip(calculateOffset(offset, limit)).sort(parseSortQuery(sortBy)).limit(parseInt(limit))
}

const getItems = (manufacturer, brand, city, state, kmFrom = 0, kmTo = 100000, priceFrom = 0, priceTo = 400000, yearFrom = 2010, yearTo = 2021, advertiserType, limit = 10, offset = 1, sortBy = undefined) => {
    const query = GET_ITEMS_QUERY(manufacturer, brand, city, state, kmFrom, kmTo, priceFrom, priceTo, yearFrom, yearTo, advertiserType, limit, offset, sortBy)

    return countDocuments(query)
            .then(count => {
                return callGetItemsRepository(query, limit, offset, sortBy)
                        .then(r => {
                            return parseResult(r, count, limit, offset)
                        })
                        .catch(e => {
                            console.log('Erro no banco: ', e)
                            return Promise.reject(e)
                        })
            })
            .catch(e => {
                return Promise.reject(e)
            })
}

const countItems = (manufacturer, brand, city, state, kmFrom, kmTo, priceFrom, priceTo, yearFrom, yearTo, advertiserType) => {
    return itemModel.aggregate(COUNT_ITEMS_QUERY(manufacturer, brand, city, state, kmFrom, kmTo, priceFrom, priceTo, yearFrom, yearTo, advertiserType))
            .then(r => {
                return r
            })
            .catch(e => {
                console.log('Erro ao fazer COUNT: ', e)
                Promise.reject(e)
            })
}

const saveItems = (items) => {

    const inserts = items.map(item => {
        item.updatedAt = new Date()
        if (item.createdAt === null || item.createdAt === undefined){
            delete item.createdAt

        }
        let result = itemModel.findOneAndUpdate({externalId: item.externalId}, item, {upsert: true})
        console.log(JSON.stringify(result))
        return result
    })

    return Promise.all(inserts)
            .then(r => {
                console.log('Items salvos com sucesso')
                return r
            })
            .catch(e => {
                console.log('Erro ao salvar items: ', e)
                return Promise.reject(e)
            })
}

export {
    getItems,
    countItems,
    saveItems
}