import itemHistoryModel from "../model/itemHistoryModel.mjs"

const saveItemsHistory = (items) => {
    return itemHistoryModel.insertMany(items)
            .then(r => {
                console.log('Items History inseridos com sucesso: ')
                return r
            })
            .catch(e => {
                console.log('Erro ao inserir items de na tabela de history: ', e)
                Promise.reject(e)
            })
}

export {
    saveItemsHistory
}