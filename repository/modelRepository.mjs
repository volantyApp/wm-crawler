import modelModel from "../model/modelModel.mjs"

const saveModels = (models) => {
    return modelModel.insertMany(models)
            .then(r => {
                console.log('Models inseridos com sucesso: ')
                return r
            })
            .catch(e => {
                console.log('Erro ao inserir Models: ', e)
                Promise.reject(e)
            })
}

const getModels = (brand) => {
    return modelModel.find({brand: brand})
            .then(r => {
                return r.map(m => m.model)
            })
            .catch(e => {
                console.log('Erro ao recuperar as Models: ', e)
                Promise.reject(e)
            })
}

export {
    saveModels,
    getModels
}