import cityModel from "../model/cityModel.mjs"

const saveCities = (cities) => {
    return cityModel.insertMany(cities)
            .then(r => {
                console.log('Cities inseridos com sucesso: ')
                return r
            })
            .catch(e => {
                console.log('Erro ao inserir Cities: ', e)
                Promise.reject(e)
            })
}

const GET_CITIES_QUERY = (state) => {
    return {state: state}
}

const getCities = (state) => {
    return cityModel.find(GET_CITIES_QUERY(state))
            .then(r => {
                return r.map(c => c.city)
            })
            .catch(e => {
                console.log('Erro ao recuperar as Cities: ', e)
                Promise.reject(e)
            })
}

export {
    saveCities,
    getCities
}