'use strict'

import {saveModels, getModels as models} from '../repository/modelRepository.mjs'
import {getBrands} from './brandService.mjs'
import {getModelsToUpdate} from './webmotorsService.mjs'

const getModels = (req) => {
    return models(req.query.brand)
}

const callModelsApi = (brands) => {
    const calls = brands.map(brand => getModelsToUpdate(brand))
    return Promise.all(calls)
            .then(r => {
                return r
            })
            .catch(e => {
                console.log('ERRO: ', e)
                return Promise.reject(e)
            })
}

const updateAllModels = (models) => {
    const flatten = models.reduce( (flat, next) => {
        return flat.concat(next)
    }, [])

    return saveModels(flatten)
}

const updateModels = () => {
    return getBrands()
            .then(callModelsApi)
            .then(updateAllModels)

}

export {
    getModels,
    updateModels
}