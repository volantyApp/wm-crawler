'use strict'

import {getItems as items, countItems as count, saveItems} from '../repository/itemRepository.mjs'
import {saveItemsHistory} from '../repository/itemHistoryRepository.mjs'
import Config from '../config/config.mjs'
const config = new Config()

const getItems = (req) => {
    const {manufacturer, brand, city, state, kmFrom, kmTo, priceFrom, priceTo, yearFrom, yearTo, limit, offset, sortBy, advertiserType} = req.query

    return items(manufacturer, brand, city, state, kmFrom, kmTo, priceFrom, priceTo, yearFrom, yearTo, advertiserType, limit, offset, sortBy)
}

const parseCountResult = (result, searchParams) => {
    const localHref = new URL(`${config.API_URL}/offers`)
    localHref.search = searchParams

    if (result && result.length > 0) {
        return {
            maxUpdatedDate: result[0].maxUpdatedDate,
            minUpdatedDate: result[0].minUpdatedDate,
            total: parseInt(result[0].total),
            minPrice: parseInt(result[0].minPrice),
            maxPrice: parseInt(result[0].maxPrice),
            averagePrice: parseFloat(result[0].avgPrice),
            minKm: parseInt(result[0].minKm),
            maxKm: parseInt(result[0].maxKm),
            averageKm: parseFloat(result[0].avgKm),
            _href: localHref.href
        }
    }

    return {
        maxUpdatedDate: "",
        minUpdatedDate: "",
        total: 0,
        minPrice: 0,
        maxPrice: 0,
        averagePrice: 0,
        minKm: 0,
        maxKm: 0,
        averageKm: 0,
        _href: ""
    }

}

const countItems = (req) => {
    const {manufacturer, brand, city, state, kmFrom, kmTo, priceFrom, priceTo, yearFrom, yearTo, advertiserType} = req.query

    return count(manufacturer, brand, city, state, kmFrom, kmTo, priceFrom, priceTo, yearFrom, yearTo, advertiserType)
            .then(r => {
                return parseCountResult(r, req._parsedUrl.search)
            })

}

const sendSaveItems = (items) => {
    return Promise.all([saveItems(items), saveItemsHistory(items)])
}

export {
    getItems,
    countItems,
    sendSaveItems
}