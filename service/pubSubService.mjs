import ps from '@google-cloud/pubsub'

import Config from '../config/config.mjs'
const config = new Config()

import {createQueue} from './queueService.mjs'

const PubSub = ps.PubSub
const pubsub = new PubSub({
    projectId: config.PROJECT_ID,
    keyFilename: process.env.GOOGLE_APPLICATION_CREDENTIALS
})

const DEFAULT_WAIT_TIME = 60

const batchConfig = () => {
    return {
        batching: {
            maxMessages: 5,
            maxMilliseconds: DEFAULT_WAIT_TIME,
        }
    }
}

const callPubSub = (dataBuffer) => {
    return pubsub.topic(config.TOPIC_NAME, batchConfig())
            .publish(dataBuffer)
            .then(messageId => {
                console.log('Mensagem enviada: ', messageId)
                return Promise.resolve(messageId)
            })
            .catch(e => {
                console.log('Erro ao enviar mensagem: ', e)
                return Promise.reject(e)
            })
}

const createBuffer = (batchRequest, offset) => {
    batchRequest['offset'] = offset
    return Buffer.from(JSON.stringify(batchRequest))
}

const sendToPubSub = (batchRequest) => {
    console.log('BatchRequest: ', batchRequest)

    const pubsubQueue = createQueue(2000, callPubSub)

    for (let i = 1; i <= batchRequest.batchCount; i ++) {
        pubsubQueue.push(createBuffer(batchRequest, i))
    }

    if (batchRequest.rest) {
        pubsubQueue.push(createBuffer(batchRequest, batchRequest.batchCount + 1))
    }

    return []
}

export {
    sendToPubSub
}