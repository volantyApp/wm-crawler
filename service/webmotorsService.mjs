'use strict'

import axios from 'axios'
import URL from 'url'
import Config from '../config/config.mjs'
const config = new Config()

let CURRENT_YEAR = new Date().getFullYear()

axios.interceptors.response.use(null, (error) => {
    console.log('INIT INTERCEPTOR')

    if (error.config && error.response && error.response.status === 401) {
        return updateToken()
                .then(token => {
                    error.config.headers['Authorization'] = token
                    return axios.request(error.config)
                })
    }
  
    return Promise.reject(error)
})

const customHeader = (token = "") => {
    return {
        'Content-Type': 'application/x-www-form-urlencoded;utf-8',
        'User-Agent': 'Dalvik/1.6.0 (Linux; U; Android 4.4.4; XT1031 Build/KXB21.14-L1.40)',
        'Accept-Charset': 'UTF-8',
        'Accept': 'application/json',
        'Authorization': token
    }
}

const updateToken = () => {

    let requestConfig = {
        headers: customHeader()
    }

    let data = 'username=userAppAndroid&password=aA123456&grant_type=password'
    
    return axios.post("https://apisearch.webmotors.com.br/token", data, requestConfig)
            .then(r => {
                console.log('token gerado com sucesso')
                return 'Bearer ' + r.data.access_token
            })
            .catch(e => {
                console.log('Erro ao recuperar/gerar o token', e)
                return Promise.reject(e)
            })
}

const formatStateParam = (requestConfig, state) => {
    if (Array.isArray(state)) {
        for (let i = 0; i < state.length; i++) {
            requestConfig.params[`Estado${i + 1}`] = state[i]
        }
    } else {
        requestConfig.params['Estado1'] = state
    }
}

const formatCityParam = (requestConfig, city) => {
    if (Array.isArray(city)) {
        for (let i = 0; i < city.length; i++) {
            requestConfig.params[`Cidade${i + 1}`] = city[i]
        }
    } else {
        requestConfig.params['Cidade1'] = city
    }
}

const listedOffers = (token, manufacturer, model, state, city, kmFrom = 0, kmTo = 100000, priceFrom = 0, priceTo = 400000, yearFrom = 2011, yearTo = 2021, advertiserType, limit, offset) => {
    let requestConfig = {
        headers: customHeader(token),
        params: {
            PaginaQuantidade: limit,
            PaginaAtual: offset,
            Ordenacao: 1,
            PrecoDe: priceFrom,
            PrecoAte: priceTo,
            AnoAte: yearTo,
            AnoDe: yearFrom,
            KmAte: kmTo,
            KmDe: kmFrom,
            Marca1: manufacturer,
            TipoAnuncio: '2'
        }
    }

    if (model) {
        requestConfig.params['Modelo1'] = model
    }

    formatStateParam(requestConfig, state)

    if (city) {
        formatCityParam(requestConfig, city)
    }

    if (!isEmpty(advertiserType)) {
        requestConfig.params['Anunciante'] = advertiserType.id
    }

    return axios.get(`https://apisearch.webmotors.com.br/api/carro/anuncios`, requestConfig)
        .then(r => {
            console.log('GET Offers: ')
            return {token: token, response: r.data}
        })
        .catch(e => {
            console.log('ERRO no GET Offers: ', e)
            return Promise.reject(e)
        })
}

const getSourceTotal = (request) => {
    const {manufacturer, brand, state, city, kmFrom, kmTo, priceFrom, priceTo, yearFrom, yearTo, advertiserType} = request
    const DEFAULT_LIMIT = 1
    const DEFAULT_OFFSET = 1

    return updateToken()
            .then(token => {
                return listedOffers(token, manufacturer, brand, state, city, kmFrom, kmTo, priceFrom, priceTo, yearFrom, yearTo, advertiserType, DEFAULT_LIMIT, DEFAULT_OFFSET)
            })
            .catch(e => {
                console.log('Erro ao pegar o total: ', e)
                Promise.reject(e)
            })
}

const countListedOffers = (req) => {
    return getSourceTotal(req.query)
            .then(r => {
                const remoteUrl = URL.parse(`${config.API_URL}/updateOffers${req._parsedUrl.search}`)
                return {
                    total: r.response.Retorno.Quantidade,
                    source: 'webmotors',
                    _href: remoteUrl.href
                }
            })
            .catch(e => {
                console.log('ERROR - countListedOffers: ', e)
                Promise.reject(e)
            })
}

const getOffersTotal = (req) => {
    const params = req.query
    return getSourceTotal(params)
            .then(r => {
                return {
                    params: params,
                    token: r.token,
                    total: r.response.Retorno.Quantidade
                }
            })
            .catch(e => {
                console.log('ERROR - getOffersTotal: ', e)
                Promise.reject(e)
            })
}

const getCities = (token, state) => {
    let requestConfig = {
        headers: customHeader(token),
        params: {
            aspectname: 'City',
            parentAspect: 'State',
            parentvalue: state
        }
    }

    return axios.get('https://apisearch.webmotors.com.br/api/carro/aspect', requestConfig)
            .then(r => {
                return r.data
            })
            .catch(e => {
                console.log('Error ao recuperar Cities: ', e)
                return Promise.reject(e)
            })
}

const getCitiesToUpdate = (req) => {
    const state = req.query.state

    return updateToken()
            .then(token => {
                return getCities(token, state)
                    .then(r => {
                        return r.Retorno.map(c => {
                            return {city: c.Valor, state: state}
                        })
                    })
                    .catch(e => {
                        console.log('Erro ao recuperar as cidades: ', e)
                        return Promise.reject(e)
                    })
            })

}

const getBrands = (token) => {
    let requestConfig = {
        headers: customHeader(token),
        params: {
            aspectname: 'Make',
            AnoDe: 2011,
            AnoAte: CURRENT_YEAR,
            TipoAnuncio: 2
        }
    }

    return axios.get('https://apisearch.webmotors.com.br/api/carro/aspect', requestConfig)
            .then(r => {
                return r.data.Retorno.map(c => {
                    return {brand: c.Valor}
                })
            })
            .catch(e => {
                console.log('Error ao recuperar Brands: ', e)
                return Promise.reject(e)
            })
}

const getState = (token) => {
    let requestConfig = {
        headers: customHeader(token),
        params: {
            aspectname: 'State',
            AnoDe: 2011,
            AnoAte: CURRENT_YEAR,
            TipoAnuncio: 2
        }
    }

    return axios.get('https://apisearch.webmotors.com.br/api/carro/aspect', requestConfig)
            .then(r => {
                return r.data.Retorno.map(c => {
                    return c.Valor
                })
            })
            .catch(e => {
                console.log('Error ao recuperar States: ', e)
                return Promise.reject(e)
            })
}

const getDataToFullImport = () => {
    return updateToken()
            .then(token => {
                return Promise.all([getBrands(token), getState(token)])
                        .then(r => {
                            return {brands: r[0], states: r[1]}
                        })
                        .catch(e => {
                            return Promise.reject(e)
                        })
            })
            .catch(e => {
                console.log('Erro ao recuperar dados do full import: ', e)
            })
}

const getBrandsToUpdate = () => {
    return updateToken()
            .then(getBrands)
            .catch(e => {
                console.log('Erro ao recuperar as Brands: ', e)
                return Promise.reject(e)
            })
}

const getModels = (token, brand) => {
    let requestConfig = {
        headers: customHeader(token),
        params: {
            aspectname: 'Model',
            parentAspect: 'Make',
            parentvalue: brand,
            AnoDe: 2011,
            AnoAte: CURRENT_YEAR,
            TipoAnuncio: 2
        }
    }

    return axios.get('https://apisearch.webmotors.com.br/api/carro/aspect', requestConfig)
            .then(r => {
                return r.data.Retorno.map(c => {
                    return {model: c.Valor, brand: brand}
                })
            })
            .catch(e => {
                console.log('Erro ao recuperar os Models: ', e)
                return Promise.reject(e)
            })
}

const getModelsToUpdate = (brand) => {
    return updateToken()
            .then(token => {
                return getModels(token, brand)
            })
            .catch(e => {
                return Promise.reject(e)
            })
}

const isEmpty = (value) => {
    return !value || value === null || value === ''
}

export {
    countListedOffers,
    getOffersTotal,
    getCitiesToUpdate,
    getBrandsToUpdate,
    getModelsToUpdate,
    getDataToFullImport
}