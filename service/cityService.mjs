'use strict'

import {saveCities, getCities as cities} from '../repository/cityRepository.mjs'

const getCities = (req) => {
    return cities(req.query.state)
}

const sendSaveCities = (cities) => {
    return saveCities(cities)
}

export {
    sendSaveCities,
    getCities
}