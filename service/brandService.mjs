'use strict'

import {saveBrands, getBrands as brands} from '../repository/brandRepository.mjs'

const getBrands = () => {
    return brands()
}

const sendSaveBrands = (brands) => {
    return saveBrands(brands)
}

export {
    sendSaveBrands,
    getBrands
}