'use strict'

import {countListedOffers, getOffersTotal} from './webmotorsService.mjs'
import {getItems, countItems, sendSaveItems} from './itemService.mjs'
import {sendToPubSub} from './pubSubService.mjs'
import sha256 from "sha256"

const DEFAULT_BATCH_SIZE = 10

const getOffers = (req) => {
    return getItems(req)
            .then(r => {
                return r
            })
            .catch(e => {
                console.log('Erro ao recupear OFFERS: ', e)
                return Promise.reject(e)
            })

}

const calculateBatch = (req) => {
    let batchResponse = req.params
    const rest = req.total % DEFAULT_BATCH_SIZE
    const batchCount = (req.total - rest) / DEFAULT_BATCH_SIZE

    batchResponse['batchCount'] = batchCount
    batchResponse['limit'] = DEFAULT_BATCH_SIZE
    batchResponse['rest'] = rest
    batchResponse['token'] = req.token

    return Promise.resolve(batchResponse)
}

const updateOffers = (req) => {
    return getOffersTotal(req)
            .then(calculateBatch)
            .then(sendToPubSub)
            .catch(e => {
                console.log('Erro ao calcular o batch: ', e)
                return Promise.reject(e)
            })
}

const mountBatchToFullImport = (params) => {
    return calculateBatch(params)
            .then(sendToPubSub)
            .catch(e => {
                console.log('Erro ao calcular o batch - mountBatchToFullImport: ', e)
                return Promise.reject(e)
            })
}

const summary = (req) => {
    return Promise.all([countItems(req), countListedOffers(req)])
        .then(r => {
            return {local: r[0], remote: r[1]}
        })
        .catch(e => {
            console.log('Erro ao recupear SUMMARY: ', e)
            return Promise.reject(e)
        })
}

const createModel = (offerResponse) => {
    const items = offerResponse.offers.map(offer => {
        return {
            externalId: offer.detail['Retorno']['Id'],
            basePrice: offer.detail['Retorno']['Preco'],
            name: `${offer.detail['Retorno']['Marca']['Value']} - ${offer.detail['Retorno']['Modelo']['Value']} - ${offer.detail['Retorno']['AnoFabricacao']}/${offer.detail['Retorno']['AnoModelo']}`,
            dataSource: "webmotors",
            offerDate: offer.detail['Retorno']['DataAnuncio'],
            brand: offer.detail['Retorno']['Marca']['Value'],
            color: offer.detail['Retorno']['CorPrimaria'],
            doors: offer.detail['Retorno']['NumeroPortas'],
            gear: offer.detail['Retorno']['Cambio'],
            fuel: offer.detail['Retorno']['Combustivel'],
            bodyStyle: offer.detail['Retorno']['Carroceria'],
            extras: offer.detail['Retorno']['Opcionais'],
            model: offer.detail['Retorno']['Modelo']['Value'],
            version: offer.detail['Retorno']['Versao']['Value'],
            manufactoryYear: offer.detail['Retorno']['AnoFabricacao'],
            modelYear: offer.detail['Retorno']['AnoModelo'],
            km: offer.detail['Retorno']['Quilometragem'],
            city: offer.detail['Retorno']['Cidade'],
            state: offer.detail['Retorno']['Estado'],
            adType: offer.detail['Retorno']['TipoAnunciante'],
            offerType: offer.detail['Retorno']['TipoAnuncio'],
            contacts: offer.contact['Retorno'],
            data: JSON.stringify(offer),
            serial: sha256(JSON.stringify(offer))
        }
    })

    return Promise.resolve(items)
}

const saveOffers = (req) => {
    return createModel(req.body)
            .then(sendSaveItems)
            .then(r => {
                return Promise.resolve({message: 'OK'})
            })
            .catch(e => {
                console.log('Erro ao salvar offers: ', e)
                return Promise.reject(e)
            })
}

export {
    getOffers,
    updateOffers,
    summary,
    saveOffers,
    mountBatchToFullImport
}
