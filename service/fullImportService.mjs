import {getDataToFullImport, getOffersTotal} from './webmotorsService.mjs'
import {createQueue} from './queueService.mjs'
import {mountBatchToFullImport} from './offerService.mjs'

const shuffle = (array) => {
    var currentIndex = array.length, temporaryValue, randomIndex;
  
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    
    return array;
}

const mountParams = (list) => {
    let params = []

    list.brands.map(brand => {
        list.states.map(state => {
            params.push({manufacturer: brand.brand, state: state})
        })
    })

    return Promise.resolve(shuffle(params))
}

const fullImport = () => {
    return getDataToFullImport()
        .then(mountParams)
        .then(r => {
            const offersTotalQueue = createQueue(1000, getOffersTotal)
            const batchQueue = createQueue(6000, mountBatchToFullImport)

            r.map(param => {
                offersTotalQueue.push({query: param})
                        .on('finish', (result) => {
                            if (result.total > 0) {
                                batchQueue.push(result)
                            } else {
                                console.log('Batch sem total')
                            }
                        })
                })
        })
        .catch(e => {
            return Promise.reject(e)
        })
}

export {
    fullImport
}