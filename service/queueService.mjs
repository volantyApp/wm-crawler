import Queue from 'better-queue'

const createQueue = (delay, fn) => {
    const queue = new Queue(function (input, cb) {
        fn(input)
            .then(r => {
                cb(null, r)
            })

    },
    { afterProcessDelay: delay })
     
    return queue
}

export {
    createQueue
}