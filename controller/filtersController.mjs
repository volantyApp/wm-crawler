import express from 'express'
const router = express.Router()

import {validateModelsFilterRequest, validateCitiesFilterRequest} from '../validation/filterValidator.mjs'

import {getCitiesToUpdate, getBrandsToUpdate} from '../service/webmotorsService.mjs'
import {sendSaveCities, getCities} from '../service/cityService.mjs'
import {sendSaveBrands, getBrands} from '../service/brandService.mjs'
import {getModels, updateModels} from '../service/modelService.mjs'

router.get('/cities', function (req, res) {
    validateCitiesFilterRequest(req)
        .then(getCities)
        .then(r => {
            res.send(r)
        })
        .catch(e => {
            console.log('Erro ao processar o GET: ', e)
            res.status(400)
            res.send({httpStatus: 400, message: e})
        })
})

router.get('/updateCities', function (req, res) {
    getCitiesToUpdate(req)
        .then(r => {
            sendSaveCities(r)
                .then(c => {
                    res.send({message: 'Cities salvas com sucesso'})
                })
        })
        .catch(e => {
            console.log('Erro ao processar o GET: ', e)
            res.status(400)
            res.send({httpStatus: 400, message: e})
        })
})

router.get('/brands', function (req, res) {
    getBrands()
        .then(r => {
            res.send(r)
        })
        .catch(e => {
            console.log('Erro ao processar o GET: ', e)
            res.status(400)
            res.send({httpStatus: 400, message: e})
        })
})

router.get('/updateBrands', function (req, res) {
    getBrandsToUpdate(req)
        .then(r => {
            sendSaveBrands(r)
                .then(b => {
                    res.send({message: 'Brands salvas com sucesso'})
                })
        })
        .catch(e => {
            console.log('Erro ao processar o GET: ', e)
            res.status(400)
            res.send({httpStatus: 400, message: e})
        })
})

router.get('/models', function (req, res) {
    validateModelsFilterRequest(req)
        .then(getModels)
        .then(r => {
            res.send(r)
        })
        .catch(e => {
            console.log('Erro ao processar o GET: ', e)
            res.status(400)
            res.send({httpStatus: 400, message: e})
        })
})

router.get('/updateModels', function (req, res) {
    updateModels()
        .then(r => {
            res.send(r)
        })
        .catch(e => {
            console.log('Erro ao processar o GET: ', e)
            res.status(400)
            res.send({httpStatus: 400, message: e})
        })
})

export default router