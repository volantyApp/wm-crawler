import express from 'express'
const router = express.Router()

import {validateFilters} from '../validation/itemValidator.mjs'
import {getOffers, updateOffers, summary, saveOffers} from '../service/offerService.mjs'
import {fullImport} from '../service/fullImportService.mjs'

router.get('/updateOffers', function (req, res) {
    validateFilters(req)
        .then(updateOffers)
        .then(r => {
            res.send(r)
        })
        .catch(e => {
            res.status(400)
            res.send({httpStatus: 400, message: e})
        })
})

router.get('/offers', function (req, res) {
    validateFilters(req)
        .then(getOffers)
        .then(r => {
            res.send(r)
        })
        .catch(e => {
            console.log(e)
            res.status(400)
            res.send({httpStatus: 400, message: e})
        })
})

router.post('/offers', function (req, res) {
    saveOffers(req)
        .then(r => {
            res.send(r)
        })
        .catch(e => {
            console.log('Erro ao processar o POST: ', e)
            res.status(400)
            res.send({httpStatus: 400, message: e})
        })
})

router.get('/summary', function (req, res) {
    validateFilters(req)
        .then(summary)
        .then(r => {
            res.send(r)
        })
        .catch(e => {
            console.log('Error: ', e)
            res.status(400)
            res.send({httpStatus: 400, message: e})
        })
})

router.get('/fullImport', function (req, res) {
    fullImport()
        .then(r => {
            res.status(200)
            res.send({status: 200, message: "OK"})
        })
        .catch(e => {
            console.log('Erro ao processar o GET fullImport: ', e)
            res.status(400)
            res.send({httpStatus: 400, message: e})
        })
})

export default router