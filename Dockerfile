FROM node:10-alpine
WORKDIR /app
COPY . /app
RUN apk --no-cache --virtual build-dependencies add \
    python \
    make \
    g++ \
    && npm install \
    && apk del build-dependencies
ENV NODE_ENV=production
EXPOSE 8080
CMD ["npm","run","start"]