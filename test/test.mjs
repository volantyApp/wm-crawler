let test = {
    "externalId": "26485705",
    "basePrice": 90900.0,
    "lastPrice": 90900.0,
    "name": "teste",
    "dataSource": "webmotors",
    "offerDate":   "2018-12-26T16:12:27.667Z",
    "data": {
        "Sucesso": true,
        "Retorno": {
            "Id": 26485705,
            "TipoVeiculo": "Carro",
            "TipoAnuncio": "U",
            "Fotos": ["https://image.webmotors.com.br/app/_fotos/anuncioUsados/gigante/2018/201812/20181226/volkswagen-golf-1.4-tsi-variant-highline-16v-total-flex-4p-tiptronic-WMIMAGEM14130678989.jpg", "https://image.webmotors.com.br/app/_fotos/anuncioUsados/gigante/2018/201812/20181226/volkswagen-golf-1.4-tsi-variant-highline-16v-total-flex-4p-tiptronic-WMIMAGEM14185020335.jpg", "https://image.webmotors.com.br/app/_fotos/anuncioUsados/gigante/2018/201812/20181226/volkswagen-golf-1.4-tsi-variant-highline-16v-total-flex-4p-tiptronic-WMIMAGEM1419293005.jpg", "https://image.webmotors.com.br/app/_fotos/anuncioUsados/gigante/2018/201812/20181226/volkswagen-golf-1.4-tsi-variant-highline-16v-total-flex-4p-tiptronic-WMIMAGEM14202276779.jpg", "https://image.webmotors.com.br/app/_fotos/anuncioUsados/gigante/2018/201812/20181226/volkswagen-golf-1.4-tsi-variant-highline-16v-total-flex-4p-tiptronic-WMIMAGEM1420511945.jpg", "https://image.webmotors.com.br/app/_fotos/anuncioUsados/gigante/2018/201812/20181226/volkswagen-golf-1.4-tsi-variant-highline-16v-total-flex-4p-tiptronic-WMIMAGEM14213168185.jpg", "https://image.webmotors.com.br/app/_fotos/anuncioUsados/gigante/2018/201812/20181226/volkswagen-golf-1.4-tsi-variant-highline-16v-total-flex-4p-tiptronic-WMIMAGEM14220889757.jpg", "https://image.webmotors.com.br/app/_fotos/anuncioUsados/gigante/2018/201812/20181226/volkswagen-golf-1.4-tsi-variant-highline-16v-total-flex-4p-tiptronic-WMIMAGEM14224908960.jpg"],
            "TipoAnunciante": "Pessoa Física",
            "Nome": "RODRIGO",
            "Cidade": "Cotia",
            "Estado": "São Paulo (SP)",
            "Preco": 90900.0,
            "Latitude": -23.6026684,
            "Longitude": -46.9194693,
            "DataAnuncio": "2018-12-30T16:12:27.667Z",
            "IdRevendedor": 6162751,
            "Marca": {
                "Value": "VOLKSWAGEN",
                "Id": 5
            },
            "Modelo": {
                "Value": "GOLF",
                "Id": 689
            },
            "Versao": {
                "Value": "1.4 TSI VARIANT HIGHLINE 16V TOTAL FLEX 4P TIPTRONIC",
                "Id": 346892
            },
            "AnoModelo": 2017,
            "AnoFabricacao": 2016,
            "Quilometragem": 13000.0,
            "NumeroPortas": "4",
            "CorPrimaria": "Branco",
            "Cambio": "Automática",
            "Combustivel": "Gasolina e álcool",
            "FinalPlaca": 3.0,
            "Necessidade": [],
            "Carroceria": "Perua/SW",
            "Atributos": ["Todas as revisões feitas pela concessionária", "IPVA pago", "Licenciado"],
            "Opcionais": ["Airbag", "Alarme", "Ar quente", "Banco com regulagem de altura", "Computador de bordo", "Controle de tração", "Ar condicionado", "Controle automático de velocidade", "Sensor de chuva", "Sensor de estacionamento", "Teto solar", "Volante com regulagem de altura", "Bancos em couro", "GPS"],
            "LogoLoja": "http://img.webmotors.com.br/_banner/",
            "Guid": "600e99f2-4ae9-42c1-8b8c-97107bcc95db"
        }
    }
    
}
let res = await db.upsertObject(test);
log("INFO", res)