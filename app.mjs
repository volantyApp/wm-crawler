import bodyParser from 'body-parser'
import express from 'express'
const app = express()

import Config from './config/config.mjs'
const config = new Config()

import filtersController from './controller/filtersController.mjs'
import offersController from './controller/offersController.mjs'

import stateAdapterMiddleware from './middleware/stateAdapterMiddleware.mjs'
import advertiserAdapterMiddleware from './middleware/advertiserAdapterMiddleware.mjs'
import versioningMiddleware from './middleware/versioning.mjs'

app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

const isEmpty = (value) => {
    return !value || value === null || value === ''
}

app.use((req, res, next) => {
    Object.keys(req.query)
        .forEach(key => {
            if (isEmpty(req.query[key])) {
                req.query[key] = undefined
            }
        })

    next()
})

app.use(stateAdapterMiddleware)
app.use(advertiserAdapterMiddleware)
app.use(versioningMiddleware)

app.use('/', filtersController)
app.use('/', offersController)

app.get('/resource-status', function(req, res) {
    res.send({name: 'webmotors-crawler', version: '1.1'})
})

app.listen(config.PORT, function () {
    console.log(`WM Crawler listening on port ${config.PORT}!`);
})