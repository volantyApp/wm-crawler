class Config {
    constructor() {
        // Server
        this.PORT = process.env.PORT || 8080
        this.API_URL = process.env.API_URL || `http://localhost:${this.PORT}`
        
        // Google Cloud
        this.PROJECT_ID = process.env.PROJECT_ID || "volanty-hml"

        this.VERSIONING = process.env.VERSIONING || "/api/v1/offer"
        
        // DB Connection
        this.MONGO_ENDPOINT = process.env.MONGO_ENDPOINT || "mongodb+srv://volanty-hml:z5IU7RNCa3QusNGt@cluster0-bhrr8.gcp.mongodb.net/wm-crawler?retryWrites=true"

        // PUB SUB
        this.TOPIC_NAME = process.env.TOPIC_NAME || "wm-crawler-batch"
    }
}

export default Config