'use strict'

const isEmpty = (value) => {
    return !value || value === null || value === ''
}

const validateFilters = (request) => {
    const {manufacturer, state} = request.query

    let errors = []

    if (isEmpty(manufacturer)) {
        errors.push('Filter manufacturer é obrigatório')
    }

    if (isEmpty(state)) {
        errors.push('Filter state é obrigatório')
    }

    if (errors.length > 0) {
        return Promise.reject(errors)
    } else {
        return Promise.resolve(request)
    }

}

export {
    validateFilters
}