'use strict'

const isEmpty = (value) => {
    return !value || value === null || value === ''
}

const validateModelsFilterRequest = (request) => {    
    const {brand} = request.query

    if (isEmpty(brand)) {
        return Promise.reject('Filter brand é obrigatório')
    }

    return Promise.resolve(request)
}

const validateCitiesFilterRequest = (request) => {    
    const {state} = request.query

    console.log(state)

    if (isEmpty(state)) {
        return Promise.reject('Filter state é obrigatório')
    }

    return Promise.resolve(request)
}

export {
    validateModelsFilterRequest,
    validateCitiesFilterRequest
}