import {from} from '../domain/advertiserTypeEnum.mjs'

export default (req, res, next) => {
    const {advertiser} = req.query

    if (advertiser) {
        try {
            req.query['advertiser'] = from(advertiser).id
            req.query['advertiserType'] = from(advertiser)
        } catch (e) {
            console.log('Erro ao parserar o advertiser: ', advertiser)
            res.status(422)
            res.send({httpStatus: 422, message: e.message})
            next(e)
        }
    }

    next()
}