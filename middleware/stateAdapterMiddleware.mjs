import {from} from '../domain/stateAdapterEnum.mjs'

export default (req, res, next) => {
    const {state} = req.query

    if (state) {
        try {
            req.query['state'] = from(state).value
        } catch (e) {}
    }

    next()
}