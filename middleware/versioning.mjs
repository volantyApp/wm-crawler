import Config from '../config/config.mjs'
const config = new Config()

export default (req, res, next) => {
    if (req.url.indexOf(config.VERSIONING) > -1) {
        req.url = req.url.replace(config.VERSIONING, '')
    }
    next()
}