import mongoose from '../db/db.mjs'
const Schema = mongoose.Schema

const item = new Schema({
	serial: String,
    externalId: String,
    basePrice: Number,
    lastPrice: Number,
    name: String,
    color: String,
    doors: Number,
    gear: String,
    fuel: String,
    bodyStyle: String,
    extras: Array,
    dataSource: String,
    offerDate: Date,
    brand: String,
    model: String,
    version: String,
    city: String,
    state: String,
    adType: String,
    offerType: String,
    modelYear: Number,
    manufactoryYear: Number,
    km: Number,
    contacts: [],
    data: Object,
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
}, { collection: 'item', versionKey: false })

export default mongoose.model('item', item)