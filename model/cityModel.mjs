import mongoose from '../db/db.mjs'
const Schema = mongoose.Schema

const city = new Schema({
	city: String,
	state: String,
	uf: String
}, { collection: 'city', versionKey: false })

export default mongoose.model('city', city)