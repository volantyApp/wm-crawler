import mongoose from '../db/db.mjs'
const Schema = mongoose.Schema

const itemHistory = new Schema({
	serial: String,
    externalId: String,
    basePrice: Number,
    lastPrice: Number,
    name: String,
    dataSource: String,
    offerDate: String,
    brand: String,
    model: String,
    version: String,
    city: String,
    state: String,
    adType: String,
    offerType: String,
    modelYear: Number,
    manufactoryYear: Number,
    km: Number,
    createdAt: {type: Date, default: Date.now}
}, { collection: 'itemHistory', versionKey: false })

export default mongoose.model('itemHistory', itemHistory)