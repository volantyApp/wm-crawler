import mongoose from '../db/db.mjs'
const Schema = mongoose.Schema

const brand = new Schema({
    brand: String
}, { collection: 'brand', versionKey: false })

export default mongoose.model('brand', brand)