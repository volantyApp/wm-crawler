import mongoose from '../db/db.mjs'
const Schema = mongoose.Schema

const model = new Schema({
	model: String,
	brand: String
}, { collection: 'model', versionKey: false })

export default mongoose.model('model', model)