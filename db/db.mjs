'use strict'

import mongoose from 'mongoose'
import Config from '../config/config.mjs'
const config = new Config()

function connect() {
    mongoose.connect(config.MONGO_ENDPOINT, {
        'useFindAndModify': false,
        useNewUrlParser: true,
        useUnifiedTopology: true

    });
    return mongoose.connection;
}

try {

    let db = connect();

    db.on('error', (error) => {
        console.log('MongoDB connection error', error);
        mongoose.disconnect();
    })
    
    db.on('disconnected', () => {
        connect();
    });

    db.once('open', function() {
        console.log('MongoDB connection opened')
    })

} catch(error) {
    console.log('Erro conn ', error);
}


export default mongoose